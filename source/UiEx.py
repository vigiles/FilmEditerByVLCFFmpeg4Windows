from PyQt5 import QtCore, QtGui, QtWidgets

class SliderEx(QtWidgets.QSlider):

    signal_val_change = QtCore.pyqtSignal(int)
    signal_release = QtCore.pyqtSignal()
    signal_press = QtCore.pyqtSignal(int)
    def mousePressEvent(self, event):
        super(SliderEx, self).mousePressEvent(event)
        val = self.pixelPosToRangeValue(event.pos())
        self.signal_press.emit(val)
    def mouseReleaseEvent(self, event):
        self.signal_release.emit()
        pass
    def mouseMoveEvent(self, event):
        super(SliderEx, self).mouseMoveEvent(event)
        val = self.pixelPosToRangeValue(event.pos())
        self.signal_val_change.emit(val)
        

    def pixelPosToRangeValue(self, pos):
        opt = QtWidgets.QStyleOptionSlider()
        self.initStyleOption(opt)
        gr = self.style().subControlRect(QtWidgets.QStyle.CC_Slider, opt, QtWidgets.QStyle.SC_SliderGroove, self)
        sr = self.style().subControlRect(QtWidgets.QStyle.CC_Slider, opt, QtWidgets.QStyle.SC_SliderHandle, self)

        if self.orientation() == QtCore.Qt.Horizontal:
            sliderLength = sr.width()
            sliderMin = gr.x()
            sliderMax = gr.right() - sliderLength + 1
        else:
            sliderLength = sr.height()
            sliderMin = gr.y()
            sliderMax = gr.bottom() - sliderLength + 1;
        pr = pos - sr.center() + sr.topLeft()
        p = pr.x() if self.orientation() == QtCore.Qt.Horizontal else pr.y()
        return QtWidgets.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), p - sliderMin, sliderMax - sliderMin, opt.upsideDown)

